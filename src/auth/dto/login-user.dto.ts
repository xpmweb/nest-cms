import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, Length } from 'class-validator';

export class LoginUserDto {
  @ApiProperty({ description: '登录账号', default: 'admin' })
  @IsNotEmpty({ message: '用户名不能为空' })
  @Length(4, 12, { message: '用户名的长度应为6 ~ 12 位' })
  username: string;

  @ApiProperty({ description: '登录密码', default: '123456' })
  @IsNotEmpty({ message: '密码长度应为6 ~ 20位' })
  @Length(6, 20, { message: 'username的长度应为6 ~ 20 位' })
  password: string;
}
