import { Module } from '@nestjs/common';
import { UploadController } from './upload.controller';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { UserModule } from '../user/user.module';

@Module({
  imports: [
    MulterModule.register({
      storage: diskStorage({
        destination: './upload-files',
        filename: (req, file, cb) => {
          const fileName = `${
            new Date().getTime() + extname(file.originalname)
          }`;
          return cb(null, fileName);
        },
      }),
    }),
    UserModule,
  ],
  controllers: [UploadController],
})
export class UploadModule {}
