import {
  Controller,
  Post,
  UseInterceptors,
  UploadedFile,
  UseGuards,
  Request,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { UserService } from '../user/user.service';
import { ApiHeader, ApiOperation, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guard/jwt-auth.guard';

@ApiTags('上传 upload')
@UseGuards(JwtAuthGuard)
@ApiHeader({ name: 'Authorization', description: 'Bearer eyR5.l9.1' })
@Controller('upload')
export class UploadController {
  constructor(private readonly userService: UserService) {}

  @ApiOperation({ summary: '上传头像' })
  @Post('avatar')
  @UseInterceptors(FileInterceptor('avatar'))
  async uploadAvatar(
    @UploadedFile() avatar: Express.Multer.File,
    @Request() req,
  ) {
    console.log(avatar);
    const avatarUrl = 'http://localhost:3003/upload/avatar/' + avatar.filename;
    await this.userService.updateAvatar(req.user.username, avatarUrl);
    return { msg: '上传头像成功！', avatarUrl };
  }
}
