export class SearchUserDto {
  searchSelect: 'username' | 'email';
  searchUser: string;
}
