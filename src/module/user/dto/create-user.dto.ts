import { IsEmpty, Length } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
  @ApiProperty({ description: '用户名', default: 'a' })
  @IsEmpty()
  @Length(4, 12)
  username: string;

  @ApiProperty({ description: '密码', default: '123456' })
  @IsEmpty()
  @Length(6, 20)
  password: string;

  // @IsEmpty()
  // @IsEmail()
  // @ApiProperty({ description: '邮箱', default: '123@163.com' })
  // email: string;

  @ApiProperty({ description: '角色权限', default: [] })
  roles: string[];

  @ApiProperty({ description: '角色名称', default: '' })
  roleName: string;
  roleLevel: string;
}
