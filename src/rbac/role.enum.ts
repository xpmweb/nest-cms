export enum Role {
  // group
  ViewGroup = 'viewGroup',
  AddGroup = 'addGroup',
  DeleteGroup = 'deleteGroup',
  AddDeviceToGroup = 'addDeviceToGroup',
  DeleteDeviceFromGroup = 'deleteDeviceFromGroup',

  // 用户
  CreateUser = 'createUser',
  EditInfo = 'editInfo',
  ViewUser = 'viewUser',
  DeleteUser = 'deleteUser',

  // 角色
  EditPermission = 'editPermission',
  EditRole = 'editRole',
  CreateRole = 'createRole',
  DeleteRole = 'deleteRole',
  ViewRole = 'viewRole',
}
