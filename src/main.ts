import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { logger } from './middleware/logger.middleware';
import { AllExceptionsFilter } from './filter/all-exception.filter';
import { HttpExceptionFilter } from './filter/http-exception.filter';
import { NestExpressApplication } from '@nestjs/platform-express';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  // 监听所有的请求路由，并打印日志
  app.use(logger);

  app.useGlobalFilters(new AllExceptionsFilter());
  app.useGlobalFilters(new HttpExceptionFilter());

  app.useStaticAssets('upload-files', {
    prefix: '/upload/avatar', // 虚拟前缀
  });

  const config = new DocumentBuilder()
    .setTitle('nest-cms')
    .setDescription('nest-cms-api')
    .setVersion('1.0')
    .addTag('')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api-docs', app, document);
  await app.listen(3003);
}
bootstrap();
