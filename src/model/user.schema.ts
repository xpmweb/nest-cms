import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document, Date } from 'mongoose';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop({ required: true })
  username: string;

  // @Prop()
  // email: string;

  @Prop({ required: true })
  password: string;

  @Prop()
  introduction: string;

  @Prop()
  avatar: string;

  @Prop([String])
  roles: string[];

  @Prop()
  roleName: string;

  // @Prop()
  // roleLevel: string;

  @Prop({ type: mongoose.Schema.Types.Date })
  createdAt: Date;

  @Prop({ type: mongoose.Schema.Types.Date })
  updateAt: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);
